package com.ascendcorp.service;

import java.util.Optional;
import java.util.concurrent.ExecutionException;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import com.ascendcorp.entity.CityEntity;
import com.ascendcorp.repository.CityRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@AllArgsConstructor
@Slf4j
public class CityService {
  private CityRepository cityRepository;
  private ChildService childService;

  public CityEntity saveCity(String name) {
    CityEntity cityEntity = new CityEntity();
    cityEntity.setName(name);
    return cityRepository.save(cityEntity);
  }

//  @Cacheable(cacheNames = "city",
//      key = "T(com.ascendcorp.multitenant.ThreadLocalStorage).getTenantName() + '_' + #cityId")
//  @Cacheable(cacheNames = "city")
  public CityEntity getCity(Integer cityId) {
//    Optional<CityEntity> cityEntityOptional = cityRepository.findById(cityId);
    CityEntity cityEntity;
    try {
      cityEntity = childService.getCity(cityId).get();
    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
      return null;
    }
    return cityEntity;
  }
}
