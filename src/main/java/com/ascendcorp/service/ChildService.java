package com.ascendcorp.service;

import java.util.concurrent.CompletableFuture;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import com.ascendcorp.entity.CityEntity;
import com.ascendcorp.repository.CityRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ChildService {
  private CityRepository cityRepository;
  
  @Async
  public CompletableFuture<CityEntity> getCity(Integer cityId) {
    return CompletableFuture.completedFuture(cityRepository.findById(cityId).get());
  }

}
