package com.ascendcorp.multitenant;

import java.lang.reflect.Method;
import org.springframework.cache.interceptor.SimpleKeyGenerator;

public class TenantKeyGenerator extends SimpleKeyGenerator {
  @Override
  public Object generate(Object target, Method method, Object... params) {
    return ThreadLocalStorage.getTenant() + "_" + generateKey(params);
  }
}
