package com.ascendcorp.multitenant;

import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerInterceptor;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.springframework.kafka.listener.RecordInterceptor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
//public class TenantIndentificationKafkaInterceptor implements RecordInterceptor<Object, Object> {
//  @Override
//  public ConsumerRecord<Object, Object> intercept(ConsumerRecord<Object, Object> record) {
//    String topic = record.topic();
//    String country = topic.substring(topic.length() - 4);
//    log.info("Found tenant: {}", country);
//    return null;
//  }
//
//}

public class TenantIndentificationKafkaInterceptor implements ConsumerInterceptor<Object, Object> {
  @Override
  public void configure(Map<String, ?> configs) {
    
  }

  @Override
  public ConsumerRecords<Object, Object> onConsume(ConsumerRecords<Object, Object> records) {
    String topic = records.iterator().next().topic();
    String country = topic.substring(topic.length() - 3).toLowerCase();
    log.info("Tenant found in Kafka message: {}", country);
    ThreadLocalStorage.setTenant(country);
    return records;
  }

  @Override
  public void onCommit(Map<TopicPartition, OffsetAndMetadata> offsets) {
    ThreadLocalStorage.removeTenant();
  }

  @Override
  public void close() {
    
  }
  
}
