package com.ascendcorp.multitenant;

import com.alibaba.ttl.TransmittableThreadLocal;

public class ThreadLocalStorage {
  private static TransmittableThreadLocal<String> tenant = new TransmittableThreadLocal<>();

  public static void setTenant(String tenantId) {
    tenant.set(tenantId);
  }

  public static String getTenant() {
    return tenant.get();
  }
  
  public static void removeTenant() {
    tenant.remove();
  }
}
