package com.ascendcorp.multitenant;

import javax.sql.DataSource;
import org.hibernate.engine.jdbc.connections.spi.AbstractDataSourceBasedMultiTenantConnectionProviderImpl;
import org.springframework.stereotype.Component;
import com.ascendcorp.configuration.DataSourceConfig;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@AllArgsConstructor
public class MultiTenantConnectionProviderImpl extends AbstractDataSourceBasedMultiTenantConnectionProviderImpl {
  /**
   * 
   */
  private static final long serialVersionUID = -2599384246070527363L;
  private DataSourceConfig dataSourceProperties;
  private DataSource defaultDatasource;
  
  @Override
  protected DataSource selectAnyDataSource() {
    log.info("Select any dataSource");
    return defaultDatasource;
  }

  @Override
  protected DataSource selectDataSource(String tenantIdentifier) {
    DataSource ds = dataSourceProperties.getDataSource(tenantIdentifier);
    log.info("Select datasource from {}: {}", tenantIdentifier, ds);
    return ds;
  }

}
