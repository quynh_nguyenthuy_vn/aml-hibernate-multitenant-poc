package com.ascendcorp.multitenant;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.OncePerRequestFilter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TenantIdentificationFilter extends OncePerRequestFilter {

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
      FilterChain filterChain) throws ServletException, IOException {
    String countryCode = request.getHeader("country");
    if (countryCode != null) {
      ThreadLocalStorage.setTenant(countryCode.toLowerCase());
      log.info("Tenant found in the request: {}", countryCode);
    }
    filterChain.doFilter(request, response);
    ThreadLocalStorage.removeTenant();
  }
  
  
}
