package com.ascendcorp.multitenant;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.stereotype.Component;

@Component
public class CurrentTenantIdentifiderResolverImpl implements CurrentTenantIdentifierResolver {

  public static final String DEFAULT_TENANT_ID = "default";
  
  @Override
  public String resolveCurrentTenantIdentifier() {
    return ThreadLocalStorage.getTenant() != null ? ThreadLocalStorage.getTenant() : DEFAULT_TENANT_ID;
  }

  @Override
  public boolean validateExistingCurrentSessions() {
    return true;
  }
  
}
