package com.ascendcorp.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import lombok.Data;

@Entity
@Table(name = "city")
@Data
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CityEntity implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -6546846723215414000L;
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cityGenerator")
  @SequenceGenerator(name = "cityGenerator", sequenceName = "city_id_seq", allocationSize = 1)
  private Integer id;
  private String name;
}
