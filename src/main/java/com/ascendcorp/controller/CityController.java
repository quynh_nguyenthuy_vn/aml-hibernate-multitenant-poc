package com.ascendcorp.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ascendcorp.entity.CityEntity;
import com.ascendcorp.service.CityService;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/city")
@AllArgsConstructor
public class CityController {
  private CityService cityService;
  
  @PostMapping
  public ResponseEntity<CityEntity> createCity(@RequestBody String name) {
    CityEntity cityEntity = cityService.saveCity(name);
    return ResponseEntity.ok(cityEntity);
  }
  
  @GetMapping("{city_id}")
  public ResponseEntity<CityEntity> getCity(@PathVariable("city_id") Integer cityId) {
    CityEntity cityEntity = cityService.getCity(cityId);
    return ResponseEntity.ok(cityEntity);
  }
}
