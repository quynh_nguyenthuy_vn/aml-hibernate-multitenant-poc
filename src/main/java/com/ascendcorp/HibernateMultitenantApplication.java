package com.ascendcorp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HibernateMultitenantApplication {

	public static void main(String[] args) {
		SpringApplication.run(HibernateMultitenantApplication.class, args);
	}

}
