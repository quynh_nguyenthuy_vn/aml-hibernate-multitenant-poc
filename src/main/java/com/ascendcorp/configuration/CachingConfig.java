package com.ascendcorp.configuration;

import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.ascendcorp.multitenant.TenantKeyGenerator;

@EnableCaching
@Configuration
public class CachingConfig extends CachingConfigurerSupport {

  @Bean("tenantKeyGenerator")
  public KeyGenerator keyGenerator() {
    return new TenantKeyGenerator();
  }

}
