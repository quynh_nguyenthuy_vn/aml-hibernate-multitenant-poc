package com.ascendcorp.configuration;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Configuration;
import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties(prefix = "multitenant")
public class DataSourceConfig {
  private Map<Object, Object> datasources = new LinkedHashMap<>();

  public Map<Object, Object> getDatasources() {
    return datasources;
  }

  public void setDatasources(List<DataSourceProperties> datasources) {
    datasources.forEach(source -> this.datasources.put(source.getTenantId(), buildDataSource(source)));
  }

  private static DataSource buildDataSource(DataSourceProperties source) {
    return DataSourceBuilder.create()
        .url(source.getUrl())
        .driverClassName(source.getDriverClassName())
        .username(source.getUsername())
        .password(source.getPassword())
        .build();
  }
  
  public DataSource getDataSource(String tenantId) {
    return (DataSource) datasources.get(tenantId);
  }
  
  @Getter
  @Setter
  public static class DataSourceProperties extends org.springframework.boot.autoconfigure.jdbc.DataSourceProperties {
    private String tenantId;
  }
}
