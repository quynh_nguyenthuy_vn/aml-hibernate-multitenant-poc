//package com.ascendcorp.configuration;
//
//import org.springframework.boot.autoconfigure.kafka.ConcurrentKafkaListenerContainerFactoryConfigurer;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
//import org.springframework.kafka.core.ConsumerFactory;
//import org.springframework.kafka.listener.RecordInterceptor;
//import com.ascendcorp.multitenant.TenantIndentificationKafkaInterceptor;
//
//@Configuration
//public class KafkaListenerContainerFactoryConfig {
//
//  @Bean
//  public ConcurrentKafkaListenerContainerFactory<?, ?> kafkaListenerContainerFactory(
//      ConcurrentKafkaListenerContainerFactoryConfigurer configurer,
//      ConsumerFactory<Object, Object> kafkaConsumerFactory) {
//    ConcurrentKafkaListenerContainerFactory<Object, Object> factory =
//        new ConcurrentKafkaListenerContainerFactory<>();
//    configurer.configure(factory, kafkaConsumerFactory);
//    factory.setRecordInterceptor(recordInterceptor());
//    return factory;
//  }
//  
//  @Bean
//  public RecordInterceptor<Object, Object> recordInterceptor() {
//    return new TenantIndentificationKafkaInterceptor();
//  }
//  
//}
