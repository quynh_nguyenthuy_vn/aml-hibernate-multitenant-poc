package com.ascendcorp.configuration;

import java.util.concurrent.Executor;
import org.springframework.boot.task.TaskExecutorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import com.alibaba.ttl.threadpool.TtlExecutors;

@EnableAsync
@Configuration
public class AsyncConfig {
  private final TaskExecutorBuilder taskExecutorBuilder;

  public AsyncConfig(TaskExecutorBuilder taskExecutorBuilder) {
    this.taskExecutorBuilder = taskExecutorBuilder;
  }

  @Bean
  public Executor taskExecutor() {
    ThreadPoolTaskExecutor executor = taskExecutorBuilder.build();
    executor.initialize();
    return TtlExecutors.getTtlExecutor(executor);
  }

  @Configuration
  public class CustomizedAsyncConfigurer implements AsyncConfigurer {
    @Override
    public Executor getAsyncExecutor() {
      return taskExecutor();
    }
  }
    
}
