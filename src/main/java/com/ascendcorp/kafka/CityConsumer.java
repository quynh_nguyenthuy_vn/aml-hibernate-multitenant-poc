package com.ascendcorp.kafka;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import com.ascendcorp.entity.CityEntity;
import com.ascendcorp.service.ChildService;
import com.ascendcorp.service.CityService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@EnableBinding(Sink.class)
@Slf4j
@AllArgsConstructor
public class CityConsumer {
  private CityService cityService;
  private ChildService childService;
  
  @StreamListener(Sink.INPUT)
  public void receiveRealtimeReactive(Message<Integer> message) {
    String topic = message.getHeaders().get(KafkaHeaders.RECEIVED_TOPIC, String.class);
    Integer cityId = message.getPayload();
    log.info("ThreadID {}, ThreadName {}, Received cityId: {} in topic: {}", Thread.currentThread().getId(), Thread.currentThread().getName(), cityId, topic);
    CityEntity cityEntity = cityService.getCity(cityId);
    log.info("CityEntity: {}", cityEntity);
  }

}
