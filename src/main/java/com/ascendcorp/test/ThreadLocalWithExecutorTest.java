package com.ascendcorp.test;

import java.time.Instant;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import com.alibaba.ttl.TransmittableThreadLocal;
import com.alibaba.ttl.threadpool.TtlExecutors;

public class ThreadLocalWithExecutorTest {
  public static TransmittableThreadLocal<String> itl = new TransmittableThreadLocal<>();
  
  private static void test() {
    Executor ttlExecutor = TtlExecutors.getTtlExecutor(Executors.newFixedThreadPool(1));
    Thread parent =  Thread.currentThread();
    itl.set("VNM");
    log("Parent thread name: " + parent.getName());
    log("Parent get Inheritable thread local value after set: " + itl.get());
    Runnable child = new Runnable() {
      
      @Override
      public void run() {
        log("Child thread name: " + Thread.currentThread().getName());
        log("Child get Inheritable thread local value before sleep: " + itl.get());
        try {
          Thread.sleep(3000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        log("Child get Inheritable thread local value after sleep: " + itl.get());
      }
    };
    ttlExecutor.execute(child);
    try {
      Thread.sleep(3000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    itl.remove();
    log("Parent get Inheritable thread local value after remove: " + itl.get());
    itl.set("PHL");
    log("Parent get Inheritable thread local value after set: " + itl.get());
    ttlExecutor.execute(child);
  }
  
  private static void log(String log) {
    System.out.println(getCurrentTime() + " | " + log);
  }

  private static Instant getCurrentTime() {
    return Instant.now();
  }

  public static void main(String[] args) {
    test();
  }
}
