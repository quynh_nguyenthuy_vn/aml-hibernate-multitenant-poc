# Multitenancy databases POC for AML project
This is a POC project using database per tenant
Demo scope test multi-tenant with: 
- Controller REST API and Kafka message consume
- Spring cache and 2nd-level Hibernate cache
- Using async method
## Configuration for multi-tenant datasources:
- Create 2 databases named `test_vnm` for `VNM` tenant and `test_tha` for `THA` tenant
```yml
multitenant:
  datasources:
    - tenant-id: vnm
      url: jdbc:postgresql://localhost:5432/test_vnm
      username: postgres
      password: Welcome1
      driver-class-name: org.postgresql.Driver
    - tenant-id: tha
      url: jdbc:postgresql://localhost:5432/test_tha
      username: postgres
      password: Welcome1
      driver-class-name: org.postgresql.Driver
```
```sql
CREATE DATABASE test_tha
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'C'
    LC_CTYPE = 'C'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
CREATE DATABASE test_vnm
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'C'
    LC_CTYPE = 'C'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
```
- Create database `city` table
```sql
CREATE TABLE public.city
(
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    CONSTRAINT city_pkey PRIMARY KEY (id)
);
ALTER TABLE public.city OWNER to postgres;
```
- Configure Kafka binding to 2 topics `CITY_VNM` and `CITY_THA`
```yml
spring:
  cloud:
    stream:
      kafka:
        binder:
          brokers: localhost:9092
          auto-create-topics: true
      bindings:
        input:
          destination: CITY_VNM,CITY_THA
          group: multitenant-city-group
```
- Configure Kafka consumer interceptor
```yml
spring:
  kafka:
    consumer:
      properties:
        interceptor:
          classes: com.ascendcorp.multitenant.TenantIndentificationKafkaInterceptor
```
## Filter and interceptor to detect tenant and save to ThreadLocal
- For REST API
Implements a request filter
```java
public class TenantIdentificationFilter extends OncePerRequestFilter {

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
      FilterChain filterChain) throws ServletException, IOException {
    String countryCode = request.getHeader("country");
    if (countryCode != null) {
      ThreadLocalStorage.setTenant(countryCode.toLowerCase());
      log.info("Tenant found in the request: {}", countryCode);
    }
    filterChain.doFilter(request, response);
    ThreadLocalStorage.removeTenant();
  }
  
}

```
- For Kafka consumer
```java
public class TenantIndentificationKafkaInterceptor implements ConsumerInterceptor<Object, Object> {
  @Override
  public void configure(Map<String, ?> configs) {
    
  }

  @Override
  public ConsumerRecords<Object, Object> onConsume(ConsumerRecords<Object, Object> records) {
    String topic = records.iterator().next().topic();
    String country = topic.substring(topic.length() - 3).toLowerCase();
    log.info("Tenant found in Kafka message: {}", country);
    ThreadLocalStorage.setTenant(country);
    return records;
  }

  @Override
  public void onCommit(Map<TopicPartition, OffsetAndMetadata> offsets) {
    ThreadLocalStorage.removeTenant();
  }

  @Override
  public void close() {
    
  }
  
}
```
## Select DataSource based on tenant
Using Hibernate's `CurrentTenantIdentifierResolver` interface and `AbstractDataSourceBasedMultiTenantConnectionProviderImpl` abstract class

To resolve tenant: get tenant information from ThreadLocal
```java
@Component
public class CurrentTenantIdentifiderResolverImpl implements CurrentTenantIdentifierResolver {

  public static final String DEFAULT_TENANT_ID = "default";
  
  @Override
  public String resolveCurrentTenantIdentifier() {
    return ThreadLocalStorage.getTenant() != null ? ThreadLocalStorage.getTenant() : DEFAULT_TENANT_ID;
  }

  @Override
  public boolean validateExistingCurrentSessions() {
    return true;
  }
  
}
```
To select DataSource by resolved tenant:
```java
@Slf4j
@Component
@AllArgsConstructor
public class MultiTenantConnectionProviderImpl extends AbstractDataSourceBasedMultiTenantConnectionProviderImpl {
  
  private static final long serialVersionUID = -2599384246070527363L;
  private DataSourceConfig dataSourceProperties;
  private DataSource defaultDatasource;
  
  @Override
  protected DataSource selectAnyDataSource() {
    log.info("Select any dataSource");
    return defaultDatasource;
  }

  @Override
  protected DataSource selectDataSource(String tenantIdentifier) {
    DataSource ds = dataSourceProperties.getDataSource(tenantIdentifier);
    log.info("Select datasource from {}: {}", tenantIdentifier, ds);
    return ds;
  }

}
```
## Handling Spring caching and 2nd-level Hibernate caching
Override the Spring default `SimpleKeyGenerator` class to generate key with tenant
```java
public class TenantKeyGenerator extends SimpleKeyGenerator {
  @Override
  public Object generate(Object target, Method method, Object... params) {
    return ThreadLocalStorage.getTenant() + "_" + generateKey(params);
  }
}

@EnableCaching
@Configuration
public class CachingConfig extends CachingConfigurerSupport {

  @Bean("tenantKeyGenerator")
  public KeyGenerator keyGenerator() {
    return new TenantKeyGenerator();
  }

}
```
## Supporting inheritable thread-local variables in child threads
Using `TransmittableThreadLocal` so that thread-local variables can be transferred to child threads when parent thread calling `@Async` method:

```xml
<dependency>
	<groupId>com.alibaba</groupId>
	<artifactId>transmittable-thread-local</artifactId>
	<version>2.12.1</version>
</dependency>
```
```java
public class ThreadLocalStorage {
  private static TransmittableThreadLocal<String> tenant = new TransmittableThreadLocal<>();

  public static void setTenant(String tenantId) {
    tenant.set(tenantId);
  }

  public static String getTenant() {
    return tenant.get();
  }
  
  public static void removeTenant() {
    tenant.remove();
  }
}
```
Use the Transmittable's `TtlExecutors` to wrap the `java.util.concurrent.Executor` to manage thread pool
- To override the Spring default's `ThreadPoolTaskExecutor`
```java
@EnableAsync
@Configuration
public class AsyncConfig {
  private final TaskExecutorBuilder taskExecutorBuilder;

  public AsyncConfig(TaskExecutorBuilder taskExecutorBuilder) {
    this.taskExecutorBuilder = taskExecutorBuilder;
  }

  @Bean
  public Executor taskExecutor() {
    ThreadPoolTaskExecutor executor = taskExecutorBuilder.build();
    executor.initialize();
    return TtlExecutors.getTtlExecutor(executor);
  }

  @Configuration
  public class CustomizedAsyncConfigurer implements AsyncConfigurer {
    @Override
    public Executor getAsyncExecutor() {
      return taskExecutor();
    }
  }
    
}
```
- Or to use in any task that needs thread pool
```java
public static TransmittableThreadLocal<String> itl = new TransmittableThreadLocal<>();
```
```java
Executor ttlExecutor = TtlExecutors.getTtlExecutor(Executors.newFixedThreadPool(1));

Runnable child = new Runnable() {      
  @Override
  public void run() {
    log("Child thread name: " + Thread.currentThread().getName());
    log("Child get Inheritable thread local value: " itl.get());
  }
};

ttlExecutor.execute(child);
```